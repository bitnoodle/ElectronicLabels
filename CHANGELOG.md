# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.0.1] - 2021-07-03

Initial release for resistor labels with template configurations for
- Avery 6450 1" Round
- Avery 6467 1/2" x 1-3/4" ID Labels
