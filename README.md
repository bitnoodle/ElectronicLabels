# Electronic Label Generator
Generate printable label sheets for electronics.  Only supports resisistors
with color codes at this time.  Useful for labeling component storage containers.
Only the resistor value bands without the tolerance band will be output.
## Usage
```
usage: Resistors -cf <file> -of <file> [options] [resistor values]
-h,--help                 show help
-cf,--configFile <arg>    * label configuration file
-of,--outputFile <arg>    * output file name
-sr,--startRow <arg>      starting label sheet row
-sc,--startColumn <arg>   starting label sheet column
-if,--inputFile <arg>     read resistor values from input file
-st,--showTemplate        show the template in the output
-t,--templateFile <arg>   path to template file (override's config value)
-bb,--boundingBox         show the label's bounding box
-sg,--showGrid <arg>      show a grid every <arg> points
```

## Resistor Configuration file
Some trial and error is necessary here.  It helps to use a template file with the _--boundingBox_, _--showTemplate_, and _--showGrid_ options.  Avery templates can be downloaded from the [Avery Templates](https://www.avery.com/templates) site.
The templates are only needed if you are using the _--showTemplate_ option and wand to see how the output looks against the template.

- templateFile: the template file this properties file is associated with (required if _--showTemplate_ option provided)
- offsetOrigin: where all offsets are calculated from for each label - note that offsets can be negative and when using CENTER, then offsets are from center of label to center of positioned element (CENTER|LOWER_LEFT)
- first.offset.left: distance units from the left of the page where the first label starts
- first.offset.top: distance units from the top of the page where the first label starts
- label.width: width of each label
- label.height: height of each label
- label.spacing.x: horizontal distance units between labels (offset origin to offset origin)
- label.spacing.y: vertical distance units between labels (offset origin to offset origin)
- columns: number of labels in each row
- rows: number of rows
- bs1852.font.size: font size for labels using BS 1852 resistor coding
- bs1852.offset.x: horizontal offset for BS 1852 resistor value label
- bs1852.offset.y: vertical offset for BS 1852 resistor value label
- decimal.font.size: font size for labels using decimal value resistor coding
- decimal.offset.x: horizontal offset for decimal resistor value label
- decimal.offset.y: vertical offset for decimal resistor value label
- bands.width: width of resistor bands
- bands.spacing: spacing between resistor bands
- bands.single.offset.x: horizontal offset for a single resistor band
- bands.single.offset.y: vertical offset for a single resistor band
- bands.single.offset.height: height of a single resistor band
- bands.4.offset.x: horizontal offset for when there are 4 resistor bands
- bands.4.offset.y: vertical offset for when there are 4 resistor bands
- bands.4.height: height of resistor bands when there are 4
- bands.5.offset.x: horizontal offset for when there are 5 resistor bands
- bands.5.offset.y: vertical offset for when there are 5 resistor bands
- bands.5.height: height of resistor bands when there are 5

## Currently configured Avery templates
- [6450 - Round Labels - 1" diameter - 63 per Sheet, White](https://www.avery.com/templates/6450)
- [6467 - ID Labels - 1/2" x 1-3/4" - 80 per Sheet, White](https://www.avery.com/templates/6467)
