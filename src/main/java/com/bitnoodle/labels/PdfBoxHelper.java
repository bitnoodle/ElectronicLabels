/*
 *     Copyright (c) 2019-present, Bitnoodle/ElectronicLabels contributers.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.bitnoodle.labels;

import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDFontDescriptor;
import org.apache.pdfbox.pdmodel.graphics.color.PDColor;
import org.apache.pdfbox.util.Matrix;

import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
import java.io.IOException;

public class PdfBoxHelper {

    public static float toFontValue(float value, float fontSize) {
        return value / 1000f * fontSize;
    }

    public static void drawBox(PDPageContentStream cs, float x, float y, float width, float height, PDColor boxColor)
            throws IOException {
        cs.addRect(x, y, width, height);
        // cs.setNonStrokingColor(intToCmykColor(band));
        cs.setStrokingColor(boxColor);
        cs.stroke();
    }

    public static void drawCrossAt(PDPageContentStream cs, float x, float y, float radius, PDColor color)
            throws IOException {
        PdfBoxHelper.drawLine(cs, x - radius, y, x + radius, y, color);
        PdfBoxHelper.drawLine(cs, x, y - radius, x, y + radius, color);
    }

    public static void drawLine(PDPageContentStream cs, float startX, float startY, float endX, float endY,
                                PDColor color) throws IOException {
        cs.moveTo(startX, startY);
        cs.lineTo(endX, endY);
        cs.setStrokingColor(color);
        cs.stroke();
    }

    public static void drawTextCentered(PDPageContentStream cs, String text, PDFont font, float fontSize, float x,
                                        float y, float rotation, PDColor fontColor) throws IOException {
        PDFontDescriptor fd = font.getFontDescriptor();

        float textWidth = font.getStringWidth(text) / 1000f * fontSize;
        float textHeight = fd.getCapHeight() / 1000f * fontSize;

        Matrix m = Matrix.getRotateInstance(rotation, 0, 0);
        PDRectangle rect = new PDRectangle(0, 0, textWidth, textHeight);
        GeneralPath gp = rect.transform(m);
        Rectangle2D rect2D = gp.getBounds2D();

        cs.beginText();
        cs.setFont(font, fontSize);
        cs.setNonStrokingColor(fontColor);
        cs.newLineAtOffset(x, y);

        m = Matrix.getRotateInstance(rotation, x - (float) rect2D.getCenterX(), y - (float) rect2D.getCenterY());
        cs.setTextMatrix(m);

        cs.showText(text);
        cs.endText();
    }

    public static void drawTextCentered(PDPageContentStream cs, String text, PDFont font, float fontSize, float x,
                                        float y, PDColor fontColor) throws IOException {

        PDFontDescriptor fd = font.getFontDescriptor();

        float textWidth = font.getStringWidth(text) / 1000f * fontSize;
        float textHeight = fd.getCapHeight() / 1000f * fontSize;

        cs.beginText();
        cs.setFont(font, fontSize);
        cs.setNonStrokingColor(fontColor);
        cs.newLineAtOffset(x - (textWidth / 2f), y - (textHeight / 2f));

        cs.showText(text);
        cs.endText();
    }

    public static void drawText(PDPageContentStream cs, String text, PDFont font, float fontSize, float x,
                                float y, float rotation, PDColor fontColor) throws IOException {
        PDFontDescriptor fd = font.getFontDescriptor();

        float textWidth = font.getStringWidth(text) / 1000f * fontSize;
        float textHeight = fd.getCapHeight() / 1000f * fontSize;

        Matrix m = Matrix.getRotateInstance(rotation, 0, 0);
        PDRectangle rect = new PDRectangle(0, 0, textWidth, textHeight);
        GeneralPath gp = rect.transform(m);
        Rectangle2D rect2D = gp.getBounds2D();

        cs.beginText();
        cs.setFont(font, fontSize);
        cs.setNonStrokingColor(fontColor);
        cs.newLineAtOffset(x, y);

        m = Matrix.getRotateInstance(rotation, x - (float) rect2D.getMinX(), y - (float) rect2D.getMinY());
        cs.setTextMatrix(m);

        cs.showText(text);
        cs.endText();
    }

    public static void drawText(PDPageContentStream cs, String text, PDFont font, float fontSize, float x, float y,
                                PDColor fontColor) throws IOException {

        cs.beginText();
        cs.setFont(font, fontSize);
        cs.setNonStrokingColor(fontColor);
        cs.newLineAtOffset(x, y);

        cs.showText(text);
        cs.endText();
    }
}
