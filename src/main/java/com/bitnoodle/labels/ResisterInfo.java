/*
 *     Copyright (c) 2019-present, Bitnoodle/ElectronicLabels contributers.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.bitnoodle.labels;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ResisterInfo {
    public static final BigDecimal K = BigDecimal.valueOf(1_000);
    public static final BigDecimal M = BigDecimal.valueOf(1_000_000);
    private static final Pattern decimalPattern = Pattern.compile("^(\\d*(?:\\.\\d*)?)([KM])?$");
    private static final Pattern bs1852Pattern = Pattern.compile("^\\d*([RKM])\\d*$");
    private String bs1852;
    private String decimal;
    private int[] colorBands4 = null;
    private int[] colorBands5 = null;

    public ResisterInfo() {

    }

    public ResisterInfo(String rValue) {
//		System.out.println("Parsing " + rValue);
        String abbrev;
        BigDecimal value;
        Matcher dm = decimalPattern.matcher(rValue);
        Matcher m = bs1852Pattern.matcher(rValue);
        if (m.find()) {
//			System.out.println("BS1852");
            bs1852 = rValue;
            abbrev = m.group(1);
            decimal = bs1852.replace(abbrev, ".");
            if (decimal.endsWith(".")) {
                decimal = decimal.substring(0, decimal.length() - 1);
            }
            value = new BigDecimal(decimal);
        } else if (dm.find()) {
//			System.out.println("DECIMAL");
            value = new BigDecimal(dm.group(1));
            abbrev = dm.group(2);
            if (abbrev == null) {
                abbrev = "R";
            }
            decimal = value.stripTrailingZeros().toPlainString();
            if (decimal.length() > 1 && decimal.startsWith("0")) {
                decimal = decimal.substring(1);
            }
            if (decimal.contains(".")) {
                bs1852 = decimal.replace(".", abbrev);
            } else {
                bs1852 = decimal + abbrev;
            }
        } else {
            throw new IllegalArgumentException("Unknown resister encoding: " + rValue);
        }
        if (!abbrev.equals("R")) {
            decimal = decimal + abbrev;
            if (abbrev.equals("K")) {
                value = value.multiply(K).stripTrailingZeros();
            } else if (abbrev.equals("M")) {
                value = value.multiply(M).stripTrailingZeros();
            }
        }
        String v = value.toPlainString();
        int multiplierOffset = 0;
        if (v.contains(".")) {
            int nm = v.substring(v.indexOf(".") + 1).length();
            v = value.scaleByPowerOfTen(nm).stripTrailingZeros().toPlainString();
            multiplierOffset -= nm;
        }
        if (v.length() < 2) {
            // special case
            if (v.equals("0")) {
                colorBands4 = new int[1];
                colorBands4[0] = 0;
            } else if (v.equals("1")) {
                colorBands4 = new int[3];
                colorBands4[0] = 1;
                colorBands4[1] = 0;
                colorBands4[2] = -1;
                colorBands5 = new int[4];
                colorBands5[0] = 1;
                colorBands5[1] = 0;
                colorBands5[2] = 0;
                colorBands5[3] = -2;
            } else {
                throw new IllegalArgumentException("Resister value not supported: " + v);
            }
        } else {
            colorBands4 = new int[3];
            colorBands5 = new int[4];
            int mp4, mp5 = 0;

            colorBands4[0] = colorBands5[0] = v.charAt(0) - '0';
            colorBands4[1] = colorBands5[1] = v.charAt(1) - '0';
            if (v.length() > 2) {
                mp4 = v.length() - 2 + multiplierOffset;
                colorBands5[2] = v.charAt(2) - '0';
                mp5 = v.length() - 3 + multiplierOffset;
            } else {
                mp4 = multiplierOffset;
                colorBands5[2] = 0;
                mp5 = multiplierOffset - 1;
            }
            colorBands4[2] = mp4;
            if (mp5 < -2) {
                colorBands5 = null;
            } else {
                colorBands5[3] = mp5;
            }
        }
    }

    public String getBs1852() {
        return bs1852;
    }

    public void setBs1852(String bs1852) {
        this.bs1852 = bs1852;
    }

    public String getDecimal() {
        return decimal;
    }

    public void setDecimal(String decimal) {
        this.decimal = decimal;
    }

    public int[] getColorBands4() {
        return colorBands4;
    }

    public void setColorBands4(int[] colorBands4) {
        this.colorBands4 = colorBands4;
    }

    public int[] getColorBands5() {
        return colorBands5;
    }

    public void setColorBands5(int[] colorBands5) {
        this.colorBands5 = colorBands5;
    }
}
