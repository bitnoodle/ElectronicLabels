/*
 *     Copyright (c) 2019-present, Bitnoodle/ElectronicLabels contributers.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.bitnoodle.labels;

public enum ResistorColors {

    BLACK(new float[]{0.75f, 0.68f, 0.67f, 0.90f}, 0, 0, ColorTypes.CMYK),
    BROWN(new float[]{0.0f, 0.442f, 0.84f, 0.29f}, 1, 1, ColorTypes.CMYK),
    RED(new float[]{0.0f, 1.0f, 1.0f, 0.0f}, 2, 2, ColorTypes.CMYK),
    ORANGE(new float[]{0.0f, 0.353f, 1.0f, 0.0f}, 3, 3, ColorTypes.CMYK),
    YELLOW(new float[]{0.0f, 0.0f, 1.0f, 0.0f}, 4, 4, ColorTypes.CMYK),
    GREEN(new float[]{1.0f, 0.0f, 1.0f, 0.0f}, 5, 5, ColorTypes.CMYK),
    BLUE(new float[]{0.711f, 0.533f, 0.0f, 0.118f}, 6, 6, ColorTypes.CMYK),
    VIOLET(new float[]{0.299f, 1.0f, 0.0f, 0.173f}, 7, 7, ColorTypes.CMYK),
    GRAY(new float[]{0.0f, 0.0f, 0.0f, 0.337f}, 8, 8, ColorTypes.CMYK),
    WHITE(new float[]{0.0f, 0.0f, 0.0f, 0.0f}, 9, 9, ColorTypes.CMYK),

    GOLD(new float[]{0.0f, 0.207f, 0.739f, 0.204f}, -1, -1, ColorTypes.CMYK),
    SILVER(new float[]{0.0f, 0.0f, 0.0f, 0.247f}, -1, -2, ColorTypes.CMYK);

    // DK_BROWN(new float[]{0.0f, 0.337f, 0.673f, 0.604f}, 1, 1, ColorTypes.CMYK)

    private final float[] colorComponentValues;
    private final int resistorValue;
    private final int multiplierFactor;
    private final ColorTypes colorType;
    ResistorColors(float[] colorComponentValues, int resistorValue, int multiplierFactor, ColorTypes colorType) {
        this.colorComponentValues = colorComponentValues;
        this.resistorValue = resistorValue;
        this.multiplierFactor = multiplierFactor;
        this.colorType = colorType;
    }

    public static ResistorColors getFromValue(int value) {
        for (ResistorColors c : values()) {
            if (c.getResistorValue() == value) {
                return c;
            }
        }
        return null;
    }

    public static ResistorColors getFromMultiplierFactor(int value) {
        for (ResistorColors c : values()) {
            if (c.getMultiplierFactor() == value) {
                return c;
            }
        }
        return null;
    }

    public float[] getColorComponentValues() {
        return colorComponentValues;
    }

    public int getResistorValue() {
        return resistorValue;
    }

    public int getMultiplierFactor() {
        return multiplierFactor;
    }

    public ColorTypes getColorType() {
        return colorType;
    }

    enum ColorTypes {
        CMYK,
        RGB
    }
}
