/*
 *     Copyright (c) 2019-present, Bitnoodle/ElectronicLabels contributers.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.bitnoodle.labels;

import java.util.Properties;

public class ResistorConfig {
    private final String templateFile;
    private final OffsetOrigin offsetOrigin;
    private final int sheetColumns;
    private final int sheetRows;
    private final float firstOffsetLeft;
    private final float firstOffsetTop;
    private final float spacingX;
    private final float spacingY;
    private final float labelWidth;
    private final float labelHeight;
    private final ResistorColors bs1852FontColor;
    private final float bs1852FontSize;
    private final float bs1852OffsetX;
    private final float bs1852OffsetY;
    private final ResistorColors decimalFontColor;
    private final float decimalFontSize;
    private final float decimalOffsetX;
    private final float decimalOffsetY;
    private final float singleBandsOffsetX;
    private final float singleBandsOffsetY;
    private final float singleBandsHeight;
    private final float bandsWidth;
    private final float bandsSpacing;
    private final float bands4OffsetX;
    private final float bands4OffsetY;
    private final float bands4Height;
    private final float bands5OffsetX;
    private final float bands5OffsetY;
    private final float bands5Height;

    public ResistorConfig(Properties config) {
        templateFile = config.getProperty("templateFile");
        offsetOrigin = OffsetOrigin.valueOf(config.getProperty("offsetOrigin", "CENTER"));
        firstOffsetLeft = Float.parseFloat(config.getProperty("first.offset.left"));
        firstOffsetTop = Float.parseFloat(config.getProperty("first.offset.top"));
        sheetColumns = Integer.parseInt(config.getProperty("columns"));
        sheetRows = Integer.parseInt(config.getProperty("rows"));
        labelWidth = Float.parseFloat(config.getProperty("label.width"));
        labelHeight = Float.parseFloat(config.getProperty("label.height"));
        spacingX = Float.parseFloat(config.getProperty("label.spacing.x"));
        spacingY = Float.parseFloat(config.getProperty("label.spacing.y"));
        bs1852FontColor = ResistorColors.valueOf(config.getProperty("bs1852.font.color", "BLACK"));
        bs1852FontSize = Float.parseFloat(config.getProperty("bs1852.font.size"));
        bs1852OffsetX = Float.parseFloat(config.getProperty("bs1852.offset.x"));
        bs1852OffsetY = Float.parseFloat(config.getProperty("bs1852.offset.y"));
        decimalFontColor = ResistorColors.valueOf(config.getProperty("decimal.font.color", "BLACK"));
        decimalFontSize = Float.parseFloat(config.getProperty("decimal.font.size"));
        decimalOffsetX = Float.parseFloat(config.getProperty("decimal.offset.x"));
        decimalOffsetY = Float.parseFloat(config.getProperty("decimal.offset.y"));
        bandsWidth = Float.parseFloat(config.getProperty("bands.width"));
        bandsSpacing = Float.parseFloat(config.getProperty("bands.spacing"));
        singleBandsOffsetX = Float.parseFloat(config.getProperty("bands.single.offset.x"));
        singleBandsOffsetY = Float.parseFloat(config.getProperty("bands.single.offset.y"));
        singleBandsHeight = Float.parseFloat(config.getProperty("bands.single.offset.height"));
        bands4OffsetX = Float.parseFloat(config.getProperty("bands.4.offset.x"));
        bands4OffsetY = Float.parseFloat(config.getProperty("bands.4.offset.y"));
        bands4Height = Float.parseFloat(config.getProperty("bands.4.height"));
        bands5OffsetX = Float.parseFloat(config.getProperty("bands.5.offset.x"));
        bands5OffsetY = Float.parseFloat(config.getProperty("bands.5.offset.y"));
        bands5Height = Float.parseFloat(config.getProperty("bands.5.height"));
    }

    public String getTemplateFile() {
        return templateFile;
    }

    public OffsetOrigin getOffsetOrigin() {
        return offsetOrigin;
    }

    public int getSheetColumns() {
        return sheetColumns;
    }

    public int getSheetRows() {
        return sheetRows;
    }

    public float getFirstOffsetLeft() {
        return firstOffsetLeft;
    }

    public float getFirstOffsetTop() {
        return firstOffsetTop;
    }

    public float getSpacingX() {
        return spacingX;
    }

    public float getSpacingY() {
        return spacingY;
    }

    public float getLabelWidth() {
        return labelWidth;
    }

    public float getLabelHeight() {
        return labelHeight;
    }

    public ResistorColors getBs1852FontColor() {
        return bs1852FontColor;
    }

    public float getBs1852FontSize() {
        return bs1852FontSize;
    }

    public float getBs1852OffsetX() {
        return bs1852OffsetX;
    }

    public float getBs1852OffsetY() {
        return bs1852OffsetY;
    }

    public ResistorColors getDecimalFontColor() {
        return decimalFontColor;
    }

    public float getDecimalFontSize() {
        return decimalFontSize;
    }

    public float getDecimalOffsetX() {
        return decimalOffsetX;
    }

    public float getDecimalOffsetY() {
        return decimalOffsetY;
    }

    public float getBandsWidth() {
        return bandsWidth;
    }

    public float getBandsSpacing() {
        return bandsSpacing;
    }

    public float getSingleBandsOffsetX() {
        return singleBandsOffsetX;
    }

    public float getSingleBandsOffsetY() {
        return singleBandsOffsetY;
    }

    public float getSingleBandsHeight() {
        return singleBandsHeight;
    }

    public float getBands4OffsetX() {
        return bands4OffsetX;
    }

    public float getBands4OffsetY() {
        return bands4OffsetY;
    }

    public float getBands4Height() {
        return bands4Height;
    }

    public float getBands5OffsetX() {
        return bands5OffsetX;
    }

    public float getBands5OffsetY() {
        return bands5OffsetY;
    }

    public float getBands5Height() {
        return bands5Height;
    }

}
