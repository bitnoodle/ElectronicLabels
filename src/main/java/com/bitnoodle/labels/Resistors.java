/*
 *     Copyright (c) 2019-present, Bitnoodle/ElectronicLabels contributers.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.bitnoodle.labels;

import org.apache.commons.cli.*;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.color.PDColor;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceCMYK;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceRGB;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Resistors {

    public static final HashMap<ResistorColors, PDColor> COLOR_MAP = new HashMap<>();
    public static final String FONT_BOLD = "freesans-font/FreeSansBold-Xgdd.ttf";
    public static final PDFont HELVETICA_BOLD = PDType1Font.HELVETICA_BOLD;
    private static PDType0Font FREE_SANS_BOLD;
    private static ResistorConfig config;
    private static float gridSize;

    static {
        for (ResistorColors c : ResistorColors.values()) {
            if (c.getColorType() == ResistorColors.ColorTypes.CMYK) {
                COLOR_MAP.put(c, new PDColor(c.getColorComponentValues(), PDDeviceCMYK.INSTANCE));
            } else if (c.getColorType() == ResistorColors.ColorTypes.RGB) {
                COLOR_MAP.put(c, new PDColor(c.getColorComponentValues(), PDDeviceRGB.INSTANCE));
            }
        }
    }

    public static void main(String[] args) throws IOException {
        ArrayList<String> values = new ArrayList<>();

        int row = 1;
        int column = 0;
        boolean showTemplate = false;
        boolean showBoundingBox = false;
        boolean showGrid = false;
        String templateFile = null;
        String outputFile = null;

        Options options = new Options();
        options.addOption("h", "help", false, "show help");
        options.addRequiredOption("cf", "configFile", true, "* label configuration file");
        options.addRequiredOption("of", "outputFile", true, "* output file name");
        options.addOption("sr", "startRow", true, "starting label sheet row");
        options.addOption("sc", "startColumn", true, "starting label sheet column");
        options.addOption("if", "inputFile", true, "read resistor values from input file");
        options.addOption("st", "showTemplate", false, "show the template in the output");
        options.addOption("t", "templateFile", true, "path to template file (override's config value)");
        options.addOption("bb", "boundingBox", false, "show the label's bounding box");
        options.addOption("sg", "showGrid", true, "show a grid every <arg> points");

        try {
            CommandLineParser parser = new DefaultParser();
            CommandLine cmd = parser.parse(options, args);

            if (cmd.hasOption("help")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("Resistors2", options);
                System.exit(1);
            }

            if (cmd.hasOption("configFile")) {
                Properties configProps = new Properties();
                String fName = cmd.getOptionValue("configFile") + ".properties";
                InputStream is = getInputStream(fName);
                if (is == null) {
                    System.out.println("Could not open label configuration: " + fName);
                    System.exit(-1);
                }
                configProps.load(is);
                config = new ResistorConfig(configProps);
            }
            if (cmd.hasOption("startRow")) {
                row = Integer.parseInt(cmd.getOptionValue("startRow"));
            }
            if (cmd.hasOption("startColumn")) {
                column = Integer.parseInt(cmd.getOptionValue("startColumn")) - 1;
            }
            if (cmd.hasOption("outputFile")) {
                outputFile = cmd.getOptionValue("outputFile");
            }
            if (cmd.hasOption("templateFile")) {
                templateFile = cmd.getOptionValue("templateFile");
                System.out.println(templateFile);
            } else {
                if (config.getTemplateFile() != null) {
                    templateFile = config.getTemplateFile();
                }
            }

            if (cmd.hasOption("showTemplate")) {
                if (templateFile == null) {
                    System.out.println("No template file specified - ignoring --showTemplate option");
                } else {
                    showTemplate = true;
                }
            }
            if (cmd.hasOption("boundingBox")) {
                showBoundingBox = true;
            }
            if (cmd.hasOption("inputFile")) {
                String inputFile = cmd.getOptionValue("inputFile");
                values.addAll(readValuesFromFile(inputFile));
            }
            if (cmd.hasOption("showGrid")) {
                showGrid = true;
                gridSize = Float.parseFloat(cmd.getOptionValue("showGrid"));
            }
            List<String> v = cmd.getArgList();
            if (v != null) {
                values.addAll(v);
            }
        } catch (ParseException e) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.setOptionComparator(null);
            formatter.printHelp("Resistors -cf <file> -of <file> [options] [resistor values]", options);
            System.exit(-1);
        }

        PDDocument document;
        PDPage page;

        if (showTemplate) {
            InputStream templateStream = getInputStream(templateFile);
            document = PDDocument.load(templateStream);
            page = document.getPage(0);
        } else {
            document = new PDDocument();
            page = new PDPage(PDRectangle.LETTER);
            document.addPage(page);
        }

        InputStream fsb = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream(FONT_BOLD);
        FREE_SANS_BOLD = PDType0Font.load(document, fsb);
        PDRectangle pageSize = page.getMediaBox();

        float startX = config.getFirstOffsetLeft();
        float startY = pageSize.getHeight() - config.getFirstOffsetTop();
        System.out.println("pageHeight = " + pageSize.getHeight() + ", start Y position = " + startY);

        PDPageContentStream cs = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND, true,
                true);

        if (showGrid) {
            for (float y = pageSize.getHeight(); y > 0; y -= gridSize) {
                PdfBoxHelper.drawLine(cs, 0, y, pageSize.getWidth(), y, COLOR_MAP.get(ResistorColors.YELLOW));
            }
            for (float x = 0; x < pageSize.getWidth(); x += gridSize) {
                PdfBoxHelper.drawLine(cs, x, 0, x, pageSize.getHeight(), COLOR_MAP.get(ResistorColors.YELLOW));
            }
        }

        for (String value : values) {
            column++;
            if (column > config.getSheetColumns()) {
                column = 1;
                row++;
                if (row > config.getSheetRows()) {
                    row = 1;
                    cs.close();
                    page = new PDPage(PDRectangle.LETTER);
                    document.addPage(page);
                    cs = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND, true, true);
                }
            }
            float offsetOriginX = startX + ((column - 1) * config.getSpacingX());
            float offsetOriginY = startY - ((row - 1) * config.getSpacingY());
            System.out.println("\nOffset Origin (x, y): " + offsetOriginX + ", " + offsetOriginY);
            ResisterInfo ri = new ResisterInfo(value);
            writeBs1852(cs, ri.getBs1852(), offsetOriginX, offsetOriginY);
            writeDecimal(cs, ri.getDecimal(), offsetOriginX, offsetOriginY);
            writeColorBands(cs, ri.getColorBands4(), ri.getColorBands5(), offsetOriginX, offsetOriginY);
            if (showBoundingBox) {
                PdfBoxHelper.drawCrossAt(cs, offsetOriginX, offsetOriginY, 10, COLOR_MAP.get(ResistorColors.RED));
                if (config.getOffsetOrigin() == OffsetOrigin.CENTER) {
                    PdfBoxHelper.drawBox(cs, offsetOriginX - (config.getLabelWidth() / 2f),
                            offsetOriginY - (config.getLabelHeight() / 2f), config.getLabelWidth(),
                            config.getLabelHeight(), COLOR_MAP.get(ResistorColors.ORANGE));
                } else {
                    PdfBoxHelper.drawBox(cs, offsetOriginX, offsetOriginY, config.getLabelWidth(),
                            config.getLabelHeight(), COLOR_MAP.get(ResistorColors.ORANGE));
                }
            }

            System.out.println("Resistor values: " + value + " -> " + ri.getBs1852() + " / " + ri.getDecimal());

            System.out.println("4 Band Colors: " + Arrays.stream(ri.getColorBands4()).mapToObj(Resistors::intToColorName)
                    .collect(Collectors.joining(", ")));
            if (ri.getColorBands5() != null) {
                System.out.println("5 Band Colors: " + Arrays.stream(ri.getColorBands5()).mapToObj(Resistors::intToColorName)
                        .collect(Collectors.joining(", ")));
            } else {
                System.out.println("-- Not applicable to 5 color bands --");
            }
        }

        cs.close();

        document.save(outputFile);
        document.close();
    }

    private static InputStream getInputStream(String fName) throws FileNotFoundException {
        File f = new File(fName);
        InputStream is;
        if (f.exists()) {
            is = new FileInputStream(f);
        } else {
            is = Thread.currentThread().getContextClassLoader().getResourceAsStream(fName);
        }
        return is;
    }

    private static List<String> readValuesFromFile(String inputFile) throws IOException {
        LinkedList<String> values = new LinkedList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(inputFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] elements = line.split("\\s+");
                for (String value : elements) {
                    value = value.strip();
                    if (!value.isBlank()) {
                        values.add(value);
                    }
                }
            }
        }
        return values;
    }

    private static void writeBs1852(PDPageContentStream cs, String bs1852, float offsetOriginX, float offsetOriginY)
            throws IOException {
        if (config.getOffsetOrigin() == OffsetOrigin.CENTER) {
            PdfBoxHelper.drawTextCentered(cs, bs1852, HELVETICA_BOLD, config.getBs1852FontSize(),
                    offsetOriginX + config.getBs1852OffsetX(), offsetOriginY + config.getBs1852OffsetY(),
                    COLOR_MAP.get(config.getBs1852FontColor()));
        } else if (config.getOffsetOrigin() == OffsetOrigin.LOWER_LEFT) {
            PdfBoxHelper.drawText(cs, bs1852, HELVETICA_BOLD, config.getBs1852FontSize(),
                    offsetOriginX + config.getBs1852OffsetX(), offsetOriginY + config.getBs1852OffsetY(),
                    COLOR_MAP.get(config.getBs1852FontColor()));
        }
    }

    private static void writeDecimal(PDPageContentStream cs, String decimal, float offsetOriginX, float offsetOriginY)
            throws IOException {
        if (config.getOffsetOrigin() == OffsetOrigin.CENTER) {
            PdfBoxHelper.drawTextCentered(cs, decimal + " \u03a9", FREE_SANS_BOLD, config.getDecimalFontSize(),
                    offsetOriginX + config.getDecimalOffsetX(), offsetOriginY + config.getDecimalOffsetY(),
                    COLOR_MAP.get(ResistorColors.BLACK));
        } else if (config.getOffsetOrigin() == OffsetOrigin.LOWER_LEFT) {
            PdfBoxHelper.drawText(cs, decimal + " \u03a9", FREE_SANS_BOLD, config.getDecimalFontSize(),
                    offsetOriginX + config.getDecimalOffsetX(), offsetOriginY + config.getDecimalOffsetY(),
                    COLOR_MAP.get(ResistorColors.BLACK));
        }
    }

    private static void writeColorBands(PDPageContentStream cs, int[] colorBands4, int[] colorBands5,
                                        float offsetOriginX, float offsetOriginY) throws IOException {
        if (colorBands5 == null) {
            writeColorBands(cs, colorBands4, offsetOriginX + config.getSingleBandsOffsetX(),
                    offsetOriginY + config.getSingleBandsOffsetY(), config.getSingleBandsHeight());
        } else {
            writeColorBands(cs, colorBands4, offsetOriginX + config.getBands4OffsetX(),
                    offsetOriginY + config.getBands4OffsetY(), config.getBands4Height());
            writeColorBands(cs, colorBands5, offsetOriginX + config.getBands5OffsetX(),
                    offsetOriginY + config.getBands5OffsetY(), config.getBands5Height());
        }
    }

    private static void writeColorBands(PDPageContentStream cs, int[] colorBands, float offsetOriginX,
                                        float offsetOriginY, float height) throws IOException {
        float spacing = 4f;
        float width = 7f;
        float totalWidth = (colorBands.length * width) + ((colorBands.length - 1) * spacing);
        float startX = config.getOffsetOrigin() == OffsetOrigin.CENTER ? (offsetOriginX - (totalWidth / 2f))
                : offsetOriginX;
        float lowerY = config.getOffsetOrigin() == OffsetOrigin.CENTER ? (offsetOriginY - (height / 2f))
                : offsetOriginY;
        // float upperY = centerY + (height / 2f);
        for (int band : colorBands) {
            cs.addRect(startX, lowerY, width, height);
            cs.setNonStrokingColor(intToCmykColor(band));
            cs.setStrokingColor(COLOR_MAP.get(ResistorColors.BLACK));
            cs.fillAndStroke();
            startX += width + spacing;
        }
    }

    private static String intToColorName(int i) {
        ResistorColors rc = ResistorColors.getFromMultiplierFactor(i);
        if (rc != null) {
            return rc.name();
        } else {
            return "<UNKNOWN_COLOR>";
        }
    }

    private static PDColor intToCmykColor(int i) {
        return COLOR_MAP.get(ResistorColors.getFromMultiplierFactor(i));
    }
}
